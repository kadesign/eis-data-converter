import iconv from 'iconv-lite';
import { existsSync } from 'fs';
import { open } from 'node:fs/promises';
import * as path from 'path';
import * as readline from 'readline';

const rl = readline.promises.createInterface({
  input: process.stdin,
  output: process.stdout
});

const requestFile = async () => {
  let srcFilePath = await rl.question('Исходный файл: ');

  if (!srcFilePath) {
    console.log('Необходимо ввести путь к файлу!');
    return null;
  }

  srcFilePath = srcFilePath.trim().replaceAll('"', '');
  if (existsSync(srcFilePath)) {
    return srcFilePath;
  } else {
    console.log('Такого файла не существует!');
    return null;
  }
}

const readSrcMetrics = async filePath => {
  const srcFilePath = path.parse(path.resolve(filePath));
  const fileContent = await readFile(filePath);
  const rawLines = fileContent.split('\r\n');
  const metricMarker = /Точек +(\d+)/;

  for (let i = 0; i < rawLines.length; i++) {
    if (metricMarker.test(rawLines[i])) {
      const dataLines = parseInt(rawLines[i].match(metricMarker)[1]);
      if (rawLines[i + 1].includes('импеданс')) {
        const startIndex = i + 3;
        const outputFilePath = await convertImpedanceData(srcFilePath, dataLines, rawLines.slice(startIndex, startIndex + dataLines));
        console.log('Сконвертировано в файл "' + outputFilePath + '"\n');
        return;
      } else {
        i += (2 + dataLines);
      }
    }
  }
}

const convertImpedanceData = async (srcFilePath, dots, rows) => {
  let actualDots = dots;
  let output = '\r\n';
  for (let row of rows) {
    const data = row.split(' ').filter(data => !!data);

    if (data[1] >= 0) {
      output += `${toExp(data[0])} ${toExp(data[1])} ${toExp(data[2])}\r\n`;
    } else {
      actualDots--;
    }
  }
  output = actualDots + output;

  const outputFilePath = `${srcFilePath.dir}/${srcFilePath.name}_converted.txt`;
  await writeFile(outputFilePath, output);
  return outputFilePath;
}

const toExp = num => {
  const n = parseFloat(num);
  const exp = ('' + n.toExponential(14)).split('e');
  const expSign = exp[1][0];
  return `${n > 0 ? ' ' : ''}${exp[0]}E${expSign}${exp[1].slice(1).padStart(4, '0')}`;
}

const readFile = (filePath) => {
  return new Promise((resolve) => {
    open(filePath, 'r').then(file => {
      const rs = file.createReadStream().pipe(iconv.decodeStream('win1251'));

      let content;
      rs.on('data', data => {
        content = data;
        rs.destroy();
      });
      rs.on('close', () => {
        file.close().then(() => resolve(content));
      });
    });
  });
}

const writeFile = (filePath, data) => {
  return new Promise((resolve) => {
    open(filePath, 'w').then(file => {
      const ws = file.createWriteStream();
      const buff = iconv.encode(data, 'win1251');
      ws.write(buff, () => {
        file.close().then(() => resolve());
      });
    });
  });
}

const main = async () => {
  let running = true;
  let srcFilePath;

  while (running) {
    srcFilePath = undefined;
    let fileExists = false;

    while (!fileExists) {
      srcFilePath = await requestFile();
      if (srcFilePath) fileExists = true;
    }

    await readSrcMetrics(srcFilePath);
    const repeat = await rl.question('Сконвертировать другой файл? [Y (да)/любая клавиша (нет)]: ');
    if (repeat.toLowerCase().trim() !== 'y') running = false;
  }

  rl.close();
}

main().then(() => process.exit());
